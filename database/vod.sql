-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 31, 2015 at 06:57 
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vod`
--

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` int(11) NOT NULL,
  `rent_film_id` int(11) DEFAULT NULL,
  `opis` varchar(45) DEFAULT NULL,
  `zalatwione` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `rent_film_id`, `opis`, `zalatwione`) VALUES
(3, 27, 'test nowego ficzera', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `Nazwa` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `Nazwa`) VALUES
(1, 'Polska'),
(2, 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `id` int(11) NOT NULL,
  `imie` varchar(45) DEFAULT NULL,
  `nazwisko` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`id`, `imie`, `nazwisko`) VALUES
(1, 'Paweł', 'Wójcik'),
(2, 'Aleksandra', 'Gulka');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(45) DEFAULT NULL,
  `rabat` decimal(10,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `nazwa`, `rabat`) VALUES
(1, 'Brak zniżki', '0'),
(2, 'Zniżka 5%', '5');

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE `films` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(45) DEFAULT NULL,
  `director_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `price_type_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`id`, `nazwa`, `director_id`, `genre_id`, `country_id`, `discount_id`, `price_type_id`) VALUES
(3, 'Śmierć w Wenecji', 1, 1, 1, 1, 1),
(4, 'Pręgi', 2, 2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `opis` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin2;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `opis`) VALUES
(1, 'Komedia'),
(2, 'Dramat'),
(3, 'Horror');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `nazwa`) VALUES
(1, 'Przelew bankowy'),
(2, 'PayPal'),
(3, 'BitCoin');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `nazwa`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'pracownik');

-- --------------------------------------------------------

--
-- Table structure for table `price_types`
--

CREATE TABLE `price_types` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(45) DEFAULT NULL,
  `cena` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `price_types`
--

INSERT INTO `price_types` (`id`, `nazwa`, `cena`) VALUES
(1, 'Średnia półka', 6),
(2, 'Taniocha', 4);

-- --------------------------------------------------------

--
-- Table structure for table `rent_films`
--

CREATE TABLE `rent_films` (
  `id` int(11) NOT NULL,
  `termin_wypozyczenia` date DEFAULT NULL,
  `termin_konca` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cena` float DEFAULT NULL,
  `payment_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rent_films`
--

INSERT INTO `rent_films` (`id`, `termin_wypozyczenia`, `termin_konca`, `user_id`, `cena`, `payment_id`) VALUES
(27, '2017-03-01', '2015-12-30', 5, 6, 2),
(28, '2015-12-29', '2016-01-03', 5, 11.7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rent_film_entities`
--

CREATE TABLE `rent_film_entities` (
  `id` int(11) NOT NULL,
  `rent_film_id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rent_film_entities`
--

INSERT INTO `rent_film_entities` (`id`, `rent_film_id`, `film_id`) VALUES
(26, 28, 4),
(25, 27, 4),
(27, 28, 3);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `termin_poczatku` date DEFAULT NULL,
  `termin_konca` date DEFAULT NULL,
  `rabat` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `telefon` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `permission_id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `telefon`, `email`, `permission_id`, `subscription_id`) VALUES
(9, 'pawel', '$2y$10$0yWISonMGcJXRzjF4UoXFeSHFcYTm20T9BrW9656Yjie0M6KAoYtW', '909909900', 'wojcik.pawel89@gmail.com', 1, NULL),
(5, 'wojtekStapor', '$2y$10$ArtiNe2N6OIkuD8scd96i.FiddV4RE4paWYAUMlQ1L2GuF4k1Y1GW', '781725385', 'wojciech.stapor@gmail.com', 2, NULL),
(7, 'pracownik', '$2y$10$3K2OfJelJFRU//zXTmEDqex/azcNilVthSqBmrPay/uQ0a12C.3P6', '99899', 'wp@wp.pl', 3, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Reklamacje_Skladniki_Wypozyczenia1_idx` (`rent_film_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Filmy_Kraje1_idx` (`country_id`),
  ADD KEY `fk_Filmy_Typy_cen1_idx` (`price_type_id`),
  ADD KEY `fk_Filmy_Rezyserzy1_idx` (`director_id`),
  ADD KEY `fk_Filmy_Promocje1_idx` (`discount_id`),
  ADD KEY `fk_Filmy_Gatunki1_idx` (`genre_id`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_types`
--
ALTER TABLE `price_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rent_films`
--
ALTER TABLE `rent_films`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Wypozyczenia_Platnosci1_idx` (`payment_id`);

--
-- Indexes for table `rent_film_entities`
--
ALTER TABLE `rent_film_entities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Skladniki_Wypozyczenia_Filmy1_idx` (`film_id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Uzytkownicy_Prenumeraty1_idx` (`subscription_id`),
  ADD KEY `fk_Uzytkownicy_Uprawnienia1_idx` (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `price_types`
--
ALTER TABLE `price_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rent_films`
--
ALTER TABLE `rent_films`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `rent_film_entities`
--
ALTER TABLE `rent_film_entities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
