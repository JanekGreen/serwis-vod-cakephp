<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PriceTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PriceTypesTable Test Case
 */
class PriceTypesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.price_types',
        'app.films',
        'app.directors',
        'app.genres',
        'app.countries',
        'app.discounts',
        'app.rent_film_entities',
        'app.rent_films'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PriceTypes') ? [] : ['className' => 'App\Model\Table\PriceTypesTable'];
        $this->PriceTypes = TableRegistry::get('PriceTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PriceTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
