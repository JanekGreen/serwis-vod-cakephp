<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RentFilmEntitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RentFilmEntitiesTable Test Case
 */
class RentFilmEntitiesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rent_film_entities',
        'app.rent_films',
        'app.users',
        'app.permissions',
        'app.subscriptions',
        'app.payments',
        'app.films',
        'app.directors',
        'app.genres',
        'app.countries',
        'app.discounts',
        'app.price_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RentFilmEntities') ? [] : ['className' => 'App\Model\Table\RentFilmEntitiesTable'];
        $this->RentFilmEntities = TableRegistry::get('RentFilmEntities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RentFilmEntities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
