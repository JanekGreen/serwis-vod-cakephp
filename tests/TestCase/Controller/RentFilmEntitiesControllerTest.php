<?php
namespace App\Test\TestCase\Controller;

use App\Controller\RentFilmEntitiesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RentFilmEntitiesController Test Case
 */
class RentFilmEntitiesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rent_film_entities',
        'app.rent_films',
        'app.users',
        'app.permissions',
        'app.subscriptions',
        'app.payments',
        'app.films',
        'app.directors',
        'app.genres',
        'app.countries',
        'app.discounts',
        'app.price_types'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
