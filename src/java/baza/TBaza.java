/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baza;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.servlet.jsp.jstl.sql.Result;
import javax.servlet.jsp.jstl.sql.ResultSupport;

/**
 *
 * @author pawel
 */
public class TBaza {
    
    private Connection connection;
   
    
    public TBaza() throws ClassNotFoundException, SQLException
    {
      
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/vod2", "root", "");
             System.out.println("Połączono!");
  
    }
    
    public Result wykonajZapytanie(String zapytanie) throws SQLException
    {
            java.sql.Statement s = null;
        
            s = connection.createStatement();
       
            java.sql.ResultSet rezultat= s.executeQuery(zapytanie);
            Result wynik= ResultSupport.toResult(rezultat);
            
        
        return wynik;
           
    }
     public void wykonajZapytanieEdytujace(String zapytanie) throws SQLException
    {
        java.sql.Statement s = null;
        
            s = connection.createStatement();
       
            s.executeUpdate(zapytanie);
          
           
    }
    
    
    
    
}
