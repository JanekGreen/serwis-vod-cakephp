<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RentFilmEntities Controller
 *
 * @property \App\Model\Table\RentFilmEntitiesTable $RentFilmEntities
 */
class RentFilmEntitiesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index($id = null)
    {
      $user_=$this->Auth->user();
      if($user_===null){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }
      $this->paginate = [
          'contain' => ['RentFilms', 'Films']
      ];
      if($id===null)
      {

        $this->set('rentFilmEntities', $this->paginate($this->RentFilmEntities));
        $this->set('_serialize', ['rentFilmEntities']);

      } else {
        $this->set('rentFilmEntities', $this->paginate($this->RentFilmEntities->find('all',[
          'conditions'=> ['rent_film_id'=> $id ]
          ])));
            $this->set('_serialize', ['rentFilmEntities']);
      }

    }

    /**
     * View method
     *
     * @param string|null $id Rent Film Entity id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
      $user_=$this->Auth->user();
      if($user_===null){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }

        $rentFilmEntity = $this->RentFilmEntities->get($id, [
            'contain' => ['RentFilms', 'Films']
        ]);
        $this->set('rentFilmEntity', $rentFilmEntity);
        $this->set('_serialize', ['rentFilmEntity']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rentFilmEntity = $this->RentFilmEntities->newEntity();
        if ($this->request->is('post')) {
            $rentFilmEntity = $this->RentFilmEntities->patchEntity($rentFilmEntity, $this->request->data);
            if ($this->RentFilmEntities->save($rentFilmEntity, ['atomic' => false])) {
                $this->Flash->success(__('The rent film entity has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rent film entity could not be saved. Please, try again.'));
            }
        }
        $rentFilms = $this->RentFilmEntities->RentFilms->find('list', ['limit' => 200]);
        $films = $this->RentFilmEntities->Films->find('list', ['limit' => 200]);
        $this->set(compact('rentFilmEntity', 'rentFilms', 'films'));
        $this->set('_serialize', ['rentFilmEntity']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rent Film Entity id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rentFilmEntity = $this->RentFilmEntities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rentFilmEntity = $this->RentFilmEntities->patchEntity($rentFilmEntity, $this->request->data);
            if ($this->RentFilmEntities->save($rentFilmEntity, ['atomic' => false])) {
                $this->Flash->success(__('The rent film entity has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rent film entity could not be saved. Please, try again.'));
            }
        }
        $rentFilms = $this->RentFilmEntities->RentFilms->find('list', ['limit' => 200]);
        $films = $this->RentFilmEntities->Films->find('list', ['limit' => 200]);
        $this->set(compact('rentFilmEntity', 'rentFilms', 'films'));
        $this->set('_serialize', ['rentFilmEntity']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rent Film Entity id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rentFilmEntity = $this->RentFilmEntities->get($id);
        if ($this->RentFilmEntities->delete($rentFilmEntity)) {
            $this->Flash->success(__('The rent film entity has been deleted.'));
        } else {
            $this->Flash->error(__('The rent film entity could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
