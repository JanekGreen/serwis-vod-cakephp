<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Exeption\Exception;

/**
 * Genres Controller
 *
 * @property \App\Model\Table\GenresTable $Genres
 */
class GenresController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
      $user_=$this->Auth->user();
      if($user_===null ||$user_['permission_id']===2){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }
        $this->set('genres', $this->paginate($this->Genres));
        $this->set('_serialize', ['genres']);
    }

    /**
     * View method
     *
     * @param string|null $id Genre id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
      $user_=$this->Auth->user();
      if($user_===null ||$user_['permission_id']===2){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }


        $genre = $this->Genres->get($id, [
            'contain' => ['Films']
        ]);
        $this->set('genre', $genre);
        $this->set('_serialize', ['genre']);


        $u=$this->Auth->user();
        $this->set('user',$u);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $genre = $this->Genres->newEntity();
        if ($this->request->is('post')) {
            $genre = $this->Genres->patchEntity($genre, $this->request->data);
            if ($this->Genres->save($genre)) {
                $this->Flash->success(__('The genre has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The genre could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('genre'));
        $this->set('_serialize', ['genre']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Genre id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $genre = $this->Genres->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $genre = $this->Genres->patchEntity($genre, $this->request->data);
            if ($this->Genres->save($genre)) {
                $this->Flash->success(__('The genre has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The genre could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('genre'));
        $this->set('_serialize', ['genre']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Genre id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        
        $this->loadModel("Films");
        $ile=$this->Films->find('all',  [
            'conditions' => ['genre_id' => $id]
          ])->count();


           if($ile > 0)
           {
               $this->Flash->error(__('Nie można usunąć tego rekordu pownieważ jest z nim związanych '.$ile.' pozycji'));
               return $this->redirect(['action' => 'index']);
           }

        $genre = $this->Genres->get($id);

        if ($this->Genres->delete($genre)) {
            $this->Flash->success(__('The genre has been deleted.'));
        } else {
            $this->Flash->error(__('The genre could not be deleted. Please, try again.'));
        }



        return $this->redirect(['action' => 'index']);
    }
}
