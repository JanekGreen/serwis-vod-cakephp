<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */


    public function index()
    {
      $user_=$this->Auth->user();
      if($user_['permission_id']=== null ||$user_['permission_id']===2 ){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action' => 'login']);
        }

        $this->paginate = [
            'contain' => ['Permissions', 'Subscriptions']
        ];
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {

      $user_=$this->Auth->user();
      if($user_['id']!= $id ||$user_===null ){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action' => 'login']);
        }


        $user_ = $this->Users->get($id, [
            'contain' => ['Permissions', 'Subscriptions', 'RentFilms']
        ]);
        $this->set('user', $user_);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user_=$this->Auth->user();
        $user = $this->Users->newEntity();
        if($user_['permission_id']===3)
                $user['permission_id']=2; // pracownik moze dodawac tylko klientow
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $permissions = $this->Users->Permissions->find('list', ['limit' => 200]);
        $subscriptions = $this->Users->Subscriptions->find('list', ['limit' => 200]);
        $this->set(compact('user', 'permissions', 'subscriptions'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $permissions = $this->Users->Permissions->find('list', ['limit' => 200]);
        $subscriptions = $this->Users->Subscriptions->find('list', ['limit' => 200]);
        $this->set(compact('user', 'permissions', 'subscriptions'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {

        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if($user['id'] ===9)
        {
              $this->Flash->error(__('Brak uprawnień do usuniecia kozaka.'));
              return $this->redirect(['controller'=>'Users', 'action' => 'index']);;

        }

        $this->loadModel("RentFilms");
        $ile=$this->RentFilms->find('all',  [
            'conditions' => ['user_id' => $id]
          ])->count();


           if($ile > 0)
           {
               $this->Flash->error(__('Nie można usunąć tego rekordu pownieważ jest z nim związanych '.$ile.' pozycji'));
               return $this->redirect(['action' => 'index']);
           }




        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }



    public function login()
    {
      if ($this->request->is('post')) {


           $user = $this->Auth->identify();
            if ($user) {
              $this->Auth->setUser($user);
          return $this->redirect($this->Auth->redirectUrl());
    }


    $this->Flash->error(__('Podano błędne hasło proszę spróbować ponownie'));
}

}

  public function logout()
  {
    // bug naprawiony, koszyk użytkownika zostawal po wylogowaniu ;)
    $session = $this->request->session();
    $session->delete("zamowienie");

    return $this->redirect($this->Auth->logout());
  }

  public function register() {

       $user = $this->Users->newEntity();
       if ($this->request->is('post')) {
          // przez formularz rejestracyjny tworzeni są tylko uzytkownicy zwykli...

            $this->request->data['permission_id']=2;
            $this->request->data['subscription_id']=null;

            $user = $this->Users->patchEntity($user, $this->request->data);

           if (!$user->errors()) {
               if ($this->Users->save($user)) {


                   $this->Flash->success(__('Konto zostało zarejestrowane pomyślnie.'));
                   return $this->redirect(['controller' => 'Users', 'action' => 'login']);
               }
               $this->Flash->error(__('Przepraszamy, ale podczas tworzenia konta wystąpił błąd!'));
           } else {

           }

       }
       $this->set('user222', $user);

   }

}
