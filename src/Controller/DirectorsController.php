<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Directors Controller
 *
 * @property \App\Model\Table\DirectorsTable $Directors
 */
class DirectorsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
      $user_=$this->Auth->user();
      if($user_===null ||$user_['permission_id']===2){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }
        $this->set('directors', $this->paginate($this->Directors));
        $this->set('_serialize', ['directors']);
    }

    /**
     * View method
     *
     * @param string|null $id Director id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
      $user_=$this->Auth->user();
      if($user_===null ||$user_['permission_id']===2){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }
        $director = $this->Directors->get($id, [
            'contain' => ['Films']
        ]);
        $this->set('director', $director);
        $this->set('_serialize', ['director']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $director = $this->Directors->newEntity();
        if ($this->request->is('post')) {
            $director = $this->Directors->patchEntity($director, $this->request->data);
            if ($this->Directors->save($director)) {
                $this->Flash->success(__('The director has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The director could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('director'));
        $this->set('_serialize', ['director']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Director id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $director = $this->Directors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $director = $this->Directors->patchEntity($director, $this->request->data);
            if ($this->Directors->save($director)) {
                $this->Flash->success(__('The director has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The director could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('director'));
        $this->set('_serialize', ['director']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Director id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $director = $this->Directors->get($id);

        $this->loadModel("Films");
        $ileRezyserow=$this->Films->find('all',  [
            'conditions' => ['director_id' => $id]
          ])->count();


           if($ileRezyserow > 0)
           {
               $this->Flash->error(__('Nie można usunąć tego rekordu pownieważ jest z nim związanych '.$ileRezyserow.' pozycji'));
               return $this->redirect(['action' => 'index']);
           }



        if ($this->Directors->delete($director)) {
            $this->Flash->success(__('The director has been deleted.'));
        } else {
            $this->Flash->error(__('The director could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
