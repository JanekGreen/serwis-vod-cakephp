<?php namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;


/**
 * Purchases Controller
 *
 *
 */
class PurchasesController extends AppController
{

  public function index()
   {
     $user_=$this->Auth->user();
     if($user_===null ){
         $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
           return $this->redirect(['controller'=>'Users', 'action' => 'login']);
       }
      $this->render('zamow');
   }
  public function zamow($id=null)
  {
    $user_=$this->Auth->user();
      if($user_['permission_id']!==2)
      {
            $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
      }

    if($id===null)
        render('zamow');
     $zamowione= array();
     $session = $this->request->session();
     $this->loadModel('Films');
     $filmik=$this->Films->get($id,['contain' =>['Directors', 'Genres', 'Countries', 'Discounts', 'PriceTypes']]);

     if($session->read('zamowienie')===null )
     {
         $session->write('zamowienie', array($filmik));
     } else {

            $zamowione =$session->read('zamowienie');
            if(array_search($filmik, $zamowione)===false)
            {
              $zamowione[]=$filmik;
              $session->write('zamowienie', $zamowione);
            }
     }


}
public function anuluj()
  {
      $session = $this->request->session();

      $session->delete("zamowienie");
      return $this->redirect(['controller'=>'Films', 'action'=>'index']);
  }
  public function usun($id=null)
  {
    $this->loadModel('Films');
    $filmik=$this->Films->get($id,['contain' =>['Directors', 'Genres', 'Countries', 'Discounts', 'PriceTypes']]);
    $session = $this->request->session();
    $filmy=$session->read('zamowienie');
    $index = array_search($filmik, $filmy);
    if($index!==false){
    unset($filmy[$index]);
    $session->write('zamowienie',$filmy);
  }
    return $this->redirect(['controller'=>'Purchases', 'action'=>'index']);




  }
  public function podsumowanie()
  {
     if($this->request->session()->read( 'zamowienie')==null){
       return $this->redirect(['controller'=>'Purchases', 'action'=>'index']);
     }
    Time::setToStringFormat('yyyy-MM-dd');
     $teraz= Time::now();
     $sukces = true;
     $znizka = false;

    $this->set('user',$this->Auth->user());
    $this->loadModel('Payments');
    if ($this->request->is('post')) {
      $dane=$this->request->data;
      $rentFilmsTable = TableRegistry::get('RentFilms');
      $rentFilm = $rentFilmsTable->newEntity();
      $rentFilm->termin_wypozyczenia=Time::now();
      $rentFilm->termin_konca=Time::now()->modify('+5 days');
      $u=$this->Auth->user();
      $rentFilm->user_id=$u['id'];
      $rentFilm->cena=$dane['kwota'];
      $rentFilm->payment_id=$dane['Sposób_zapłaty'];
      $zamowienie= $rentFilmsTable->save($rentFilm);

      if($zamowienie !==null)
      {
          $session=$session = $this->request->session();
          $filmy=$session->read('zamowienie');
          $rentFilmsEntityTable = TableRegistry::get('RentFilmEntities');
          // transakcja jeżeli ktorykolwiek z elementow zamowienia sie nie doda to lipa i nie zrobi sie nic
          $rentFilmsEntityTable->connection()->transactional(function() use($filmy,$rentFilmsEntityTable,$zamowienie){
          $rentFilmEntity = $rentFilmsEntityTable->newEntity();

            foreach($filmy as $film)
            {
              $rentFilmEntity = $rentFilmsEntityTable->newEntity();
              $rentFilmEntity->rent_film_id=$zamowienie['id'];
              $rentFilmEntity->film_id=$film['id'];
              $sukces=$rentFilmsEntityTable->save($rentFilmEntity,['atomic'=>false]);

            }
          });


                $this->Flash->success(__('Transakcja przebiegła pomyślnie.'));
                $session->delete("zamowienie");
                return $this->redirect(['controller'=>'Films','action' => 'index']);
                } else {
                    $this->Flash->error(__('Wystąpiły błędy, spróbuj ponownie później.'));
          }
    }


    $this->set('payments',$this->Payments->find('list'));
    $this->render('podsumowanie');

  }
  public function reklamuj($id)
    {
        if ($this->request->is('post')) {
          $dane=$this->request->data;
          $complaintsEntityTable = TableRegistry::get('Complaints');
          $complaint = $complaintsEntityTable->newEntity();
          $complaint->rent_film_id=$id;
          $complaint->opis=$dane['opis'];
          $complaint->zalatwione=0;
          if($complaintsEntityTable->save($complaint))
          {
            $this->Flash->success(__('Reklamację przyjęto.'));
            return $this->redirect(['controller'=>'Films','action' => 'index']);

          } else {
             $this->Flash->error(__('Wystąpiły błędy, spróbuj ponownie później.'));

          }

        }

    }
}
?>
