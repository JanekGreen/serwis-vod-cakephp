<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Complaints Controller
 *
 * @property \App\Model\Table\ComplaintsTable $Complaints
 */
class ComplaintsController extends AppController

{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
      $user_=$this->Auth->user();
      if($user_===null ){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action' => 'login']);
        }
        $this->paginate = [
            'contain' => ['RentFilms']
        ];
        $this->set('complaints', $this->paginate($this->Complaints));
        $this->set('_serialize', ['complaints']);

    }

    /**
     * View method
     *
     * @param string|null $id Complaint id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {

      $user_=$this->Auth->user();
      if($user_['id']!= $id || $user_===null){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action' => 'login']);
        }

        $complaint = $this->Complaints->get($id, [
            'contain' => ['RentFilms']
        ]);
        $this->set('complaint', $complaint);
        $this->set('_serialize', ['complaint']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $complaint = $this->Complaints->newEntity();
        if ($this->request->is('post')) {
            $complaint = $this->Complaints->patchEntity($complaint, $this->request->data);
            if ($this->Complaints->save($complaint)) {
                $this->Flash->success(__('The complaint has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The complaint could not be saved. Please, try again.'));
            }
        }
        $rentFilms = $this->Complaints->RentFilms->find('list', ['limit' => 200]);
        $this->set(compact('complaint', 'rentFilms'));
        $this->set('_serialize', ['complaint']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Complaint id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $complaint = $this->Complaints->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $complaint = $this->Complaints->patchEntity($complaint, $this->request->data);
            if ($this->Complaints->save($complaint)) {
                $this->Flash->success(__('The complaint has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The complaint could not be saved. Please, try again.'));
            }
        }
        $rentFilms = $this->Complaints->RentFilms->find('list', ['limit' => 200]);
        $this->set(compact('complaint', 'rentFilms'));
        $this->set('_serialize', ['complaint']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Complaint id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $complaint = $this->Complaints->get($id);
        if ($this->Complaints->delete($complaint)) {
            $this->Flash->success(__('The complaint has been deleted.'));
        } else {
            $this->Flash->error(__('The complaint could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    public function showUser($id=null)
    {
       if($id===null)
       {
          $this->redirect(['controller'=>'Films', 'action'=>'index']);
       }
        $user_=$this->Auth->user();
        if($id != $user_['id']){
          $this->Flash->error(__('Nie masz uprawnień do tej operacji.'));
            $this->redirect(['controller'=>'Users', 'action'=>'login']);


        }


       $this->paginate = [
           'contain' => ['RentFilms']
       ];
       $this->set('complaints', $this->paginate($this->Complaints->find('all',['conditions'=>['user_id'=> $id]
       ])));
       $this->set('_serialize', ['complaints']);
       $this->render('index');

    }
}
