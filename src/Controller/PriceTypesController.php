<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PriceTypes Controller
 *
 * @property \App\Model\Table\PriceTypesTable $PriceTypes
 */
class PriceTypesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
      $user_=$this->Auth->user();
      if($user_===null ||$user_['permission_id']===2){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect( ['controller'=>'Users', 'action' => 'login']);
        }
        $this->set('priceTypes', $this->paginate($this->PriceTypes));
        $this->set('_serialize', ['priceTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Price Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
      $user_=$this->Auth->user();
      if($user_===null ||$user_['permission_id']!==1){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect( ['controller'=>'Films', 'action' => 'index']);
        }

        $priceType = $this->PriceTypes->get($id, [
            'contain' => ['Films', 'RentFilms']
        ]);
        $this->set('priceType', $priceType);
        $this->set('_serialize', ['priceType']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $priceType = $this->PriceTypes->newEntity();
        if ($this->request->is('post')) {
            $priceType = $this->PriceTypes->patchEntity($priceType, $this->request->data);
            if ($this->PriceTypes->save($priceType)) {
                $this->Flash->success(__('The price type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The price type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('priceType'));
        $this->set('_serialize', ['priceType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Price Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $priceType = $this->PriceTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $priceType = $this->PriceTypes->patchEntity($priceType, $this->request->data);
            if ($this->PriceTypes->save($priceType)) {
                $this->Flash->success(__('The price type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The price type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('priceType'));
        $this->set('_serialize', ['priceType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Price Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $this->loadModel("Films");
        $ile=$this->Films->find('all',  [
            'conditions' => ['price_type_id' => $id]
          ])->count();


           if($ile > 0)
           {
               $this->Flash->error(__('Nie można usunąć tego rekordu pownieważ jest z nim związanych '.$ile.' pozycji'));
               return $this->redirect(['action' => 'index']);
           }




        $priceType = $this->PriceTypes->get($id);

        if ($this->PriceTypes->delete($priceType)) {
            $this->Flash->success(__('The price type has been deleted.'));
        } else {
            $this->Flash->error(__('The price type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
