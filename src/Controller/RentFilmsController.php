<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Database\Connection;

/**
 * RentFilms Controller
 *
 * @property \App\Model\Table\RentFilmsTable $RentFilms
 */
class RentFilmsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
      $user_=$this->Auth->user();
      if($user_===null){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }

        $this->paginate = [
            'contain' => ['Users', 'Payments']
        ];
        $this->set('rentFilms', $this->paginate($this->RentFilms));
        $this->set('_serialize', ['rentFilms']);
    }
    public function showUser($u)
    {
      $user_=$this->Auth->user();
      if($user_['permission_id']===2 && $u !==$user_['username']){

          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
          return $this->redirect(['controller'=>'Users', 'action'=>'login']);
      }

      $this->paginate = [
          'contain' => ['Users', 'Payments']
      ];
      $this->set('rentFilms', $this->paginate($this->RentFilms->find('all',[
        'conditions'=> ['username'=> $u ]
        ])));
      $this->set('_serialize', ['rentFilms']);
      $this->render('index');

    }

    /**
     * View method
     *
     * @param string|null $id Rent Film id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
      $user_=$this->Auth->user();
      if($user_===null ){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }
        $rentFilm = $this->RentFilms->get($id, [
            'contain' => ['Users', 'Payments']
        ]);
        $this->set('rentFilm', $rentFilm);
        $this->set('_serialize', ['rentFilm']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rentFilm = $this->RentFilms->newEntity();
        if ($this->request->is('post')) {
            $rentFilm = $this->RentFilms->patchEntity($rentFilm, $this->request->data);
            if ($this->RentFilms->save($rentFilm, ['atomic' => true])) {
                $this->Flash->success(__('The rent film has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rent film could not be saved. Please, try again.'));
            }
        }
        $users = $this->RentFilms->Users->find('list', ['limit' => 200]);
        $payments = $this->RentFilms->Payments->find('list', ['limit' => 200]);
        $this->set(compact('rentFilm', 'users', 'payments'));
        $this->set('_serialize', ['rentFilm']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rent Film id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rentFilm = $this->RentFilms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rentFilm = $this->RentFilms->patchEntity($rentFilm, $this->request->data);
            if ($this->RentFilms->save($rentFilm)) {
                $this->Flash->success(__('The rent film has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rent film could not be saved. Please, try again.'));
            }
        }
        $users = $this->RentFilms->Users->find('list', ['limit' => 200]);
        $payments = $this->RentFilms->Payments->find('list', ['limit' => 200]);
        $this->set(compact('rentFilm', 'users', 'payments'));
        $this->set('_serialize', ['rentFilm']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rent Film id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $this->loadModel("RentFilmEntities");
        $ile=$this->RentFilmEntities->find('all',  [
            'conditions' => ['rent_film_id' => $id]
          ])->count();


           if($ile > 0)
           {
               $this->Flash->error(__('Nie można usunąć tego rekordu pownieważ jest z nim związanych '.$ile.' pozycji'));
               return $this->redirect(['action' => 'index']);
           }



        $rentFilm = $this->RentFilms->get($id);
        if ($this->RentFilms->delete($rentFilm)) {
            $this->Flash->success(__('The rent film has been deleted.'));
        } else {
            $this->Flash->error(__('The rent film could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
