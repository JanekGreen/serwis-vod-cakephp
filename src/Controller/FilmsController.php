<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Films Controller
 *
 * @property \App\Model\Table\FilmsTable $Films
 */
class FilmsController extends AppController
{


  public $components = array(
      'Search.Prg'
  );

    /**
     * Index method
     *
     * @return void
     */
     public function index($genre_id=null)
     {
       $this->set('genres',$this->Films->Genres->find('list'));
       $this->set('countries', $this->Films->countries->find('list'));
       $this->set('prices', $this->Films->PriceTypes->find('list'));


         $this->paginate = [
             'contain' => ['Directors', 'Genres', 'Countries', 'Discounts', 'PriceTypes']

         ];
         if($genre_id==null)
         {
             $this->set('films', $this->paginate($this->Films));
         }else {
           $this->set('films', $this->paginate($this->Films->find('all',
            [
              'conditions' => ['genre_id' => $genre_id]
            ]
          )));
         }
         $this->set('_serialize', ['films']);
     }


    public function search($genre_id=null)
    {
       $this->set('genres',$this->Films->Genres->find('list'));
       $this->set('countries', $this->Films->Countries->find('list'));
       $this->set('prices', $this->Films->PriceTypes->find('list'));

      $this->paginate = [
          'contain' => ['Directors', 'Genres', 'Countries', 'Discounts', 'PriceTypes']

      ];
      $this->set('films', $this->paginate($this->Films->find('all',
       [
         'conditions' => ['genre_id' => $genre_id]
       ]
     )));
      $this->Prg->commonProcess();
            $this->set('films', $this->paginate($this->Films->find('searchable', $this->Prg->parsedParams())));
    }

    /**
     * View method
     *
     * @param string|null $id Film id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
      $user_=$this->Auth->user();
      if($user_===null ||$user_['permission_id']!==1){
          $this->Flash->error(__('Brak uprawnień do oglądania tej strony.'));
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
        }

        $film = $this->Films->get($id, [
            'contain' => ['Directors', 'Genres', 'Countries', 'Discounts', 'PriceTypes', 'RentFilmEntities']
        ]);
        $this->set('film', $film);
        $this->set('_serialize', ['film']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $film = $this->Films->newEntity();
        if ($this->request->is('post')) {
            $film = $this->Films->patchEntity($film, $this->request->data);
            if ($this->Films->save($film)) {
                $this->Flash->success(__('The film has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The film could not be saved. Please, try again.'));
            }
        }
        $directors = $this->Films->Directors->find('list', ['limit' => 200]);
        $genres = $this->Films->Genres->find('list', ['limit' => 200]);
        $countries = $this->Films->Countries->find('list', ['limit' => 200]);
        $discounts = $this->Films->Discounts->find('list', ['limit' => 200]);
        $priceTypes = $this->Films->PriceTypes->find('list', ['limit' => 200]);
        $this->set(compact('film', 'directors', 'genres', 'countries', 'discounts', 'priceTypes'));
        $this->set('_serialize', ['film']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Film id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $film = $this->Films->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $film = $this->Films->patchEntity($film, $this->request->data);
            if ($this->Films->save($film)) {
                $this->Flash->success(__('The film has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The film could not be saved. Please, try again.'));
            }
        }
        $directors = $this->Films->Directors->find('list', ['limit' => 200]);
        $genres = $this->Films->Genres->find('list', ['limit' => 200]);
        $countries = $this->Films->Countries->find('list', ['limit' => 200]);
        $discounts = $this->Films->Discounts->find('list', ['limit' => 200]);
        $priceTypes = $this->Films->PriceTypes->find('list', ['limit' => 200]);
        $this->set(compact('film', 'directors', 'genres', 'countries', 'discounts', 'priceTypes'));
        $this->set('_serialize', ['film']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Film id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $this->loadModel("RentFilmEntities");
        $ile=$this->RentFilmEntities->find('all',  [
            'conditions' => ['film_id' => $id]
          ])->count();


           if($ile > 0)
           {
               $this->Flash->error(__('Nie można usunąć tego rekordu pownieważ jest z nim związanych '.$ile.' pozycji'));
               return $this->redirect(['action' => 'index']);
           }



        $film = $this->Films->get($id);
        if ($this->Films->delete($film)) {
            $this->Flash->success(__('The film has been deleted.'));
        } else {
            $this->Flash->error(__('The film could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
