<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $priceType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $priceType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Price Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Films'), ['controller' => 'Films', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Film'), ['controller' => 'Films', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Rent Films'), ['controller' => 'RentFilms', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rent Film'), ['controller' => 'RentFilms', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="priceTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($priceType) ?>
    <fieldset>
        <legend><?= __('Edit Price Type') ?></legend>
        <?php
            echo $this->Form->input('nazwa');
            echo $this->Form->input('cena');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
