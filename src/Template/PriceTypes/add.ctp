<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Html->link(__('Pokaż rodzaje cen'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokaż filmy'), ['controller' => 'Films', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowy film'), ['controller' => 'Films', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="priceTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($priceType) ?>
    <fieldset>
        <legend><?= __('Add Price Type') ?></legend>
        <?php
            echo $this->Form->input('nazwa');
            echo $this->Form->input('cena');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
