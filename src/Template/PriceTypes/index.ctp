<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Nowy typ ceny'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Pokaż filmy'), ['controller' => 'Films', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowy film'), ['controller' => 'Films', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="priceTypes index large-9 medium-8 columns content">
    <h3><?= __('Typy cen') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nazwa') ?></th>
                <th><?= $this->Paginator->sort('cena') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($priceTypes as $priceType): ?>
            <tr>
                <td><?= $this->Number->format($priceType->id) ?></td>
                <td><?= h($priceType->nazwa) ?></td>
                <td><?= $this->Number->format($priceType->cena) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edytuj'), ['action' => 'edit', $priceType->id]) ?>
                    <?= $this->Form->postLink(__('Usuń'), ['action' => 'delete', $priceType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $priceType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
