<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Pokaż wszystkie reklamacje'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokaż zamówienia'), ['controller' => 'RentFilms', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="complaints form large-9 medium-8 columns content">
    <?= $this->Form->create($complaint) ?>
    <fieldset>
        <legend><?= __('Nowa reklamacja') ?></legend>
        <?php
            echo $this->Form->input('rent_film_id', ['options' => $rentFilms, 'empty' => true]);
            echo $this->Form->input('opis');
            echo $this->Form->input('zalatwione');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
