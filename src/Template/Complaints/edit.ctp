<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $complaint->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $complaint->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Complaints'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Rent Films'), ['controller' => 'RentFilms', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rent Film'), ['controller' => 'RentFilms', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="complaints form large-9 medium-8 columns content">
    <?= $this->Form->create($complaint) ?>
    <fieldset>
        <legend><?= __('Edit Complaint') ?></legend>
        <?php
            echo $this->Form->input('rent_film_id', ['options' => $rentFilms, 'empty' => true]);
            echo $this->Form->input('opis');
            echo $this->Form->input('zalatwione');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
