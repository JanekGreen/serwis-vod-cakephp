<?php
use Cake\ORM\TableRegistry;


function findUserNameById($id)
{
  $UsersTable=TableRegistry::get('Users');
  $users=$UsersTable->find('all');

  foreach ($users as $u)
      {

        if($u['id']=== $id)
          return $u['username'];
      }

    return null;
}

 ?>

<?php if($user_['permission_id']===1 || $user_['permission_id']===3):?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Nowa reklamacja'), ['action' => 'add']) ?></li>

    </ul>
</nav>
<?php endif?>
<div class="complaints index large-9 medium-8 columns content">
    <h3><?= __('Reklamacje') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('rent_film_id','Zamówienie') ?></th>
                <th>Użytkownik</th>
                <th><?= $this->Paginator->sort('opis') ?></th>
                <th><?= $this->Paginator->sort('zalatwione') ?></th>
                <?php if($user_['permission_id']===1 || $user_['permission_id']===3):?>
                <th class="actions"><?= __('Akcje') ?></th>
              <?php endif?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($complaints as $complaint): ?>

            <tr>
                <?php if($user_['permission_id']===2 && $user_['id']!== $complaint->rent_film->user_id)
                 continue; ?>
                <td><?= $this->Number->format($complaint->id) ?></td>
                <td><?= $complaint->has('rent_film') ? $this->Html->link($complaint->rent_film->termin_wypozyczenia, ['controller' => 'RentFilms', 'action' => 'view', $complaint->rent_film->id]) : '' ?></td>
                <td><?=  findUserNameById($complaint->rent_film->user_id); ?></td>
                <td><?= h($complaint->opis) ?></td>
                <td><?= h($complaint->zalatwione == 1 ? 'Załatwione':'Oczekuje' ) ?></td>
                <?php if($user_['permission_id']===1 || $user_['permission_id']===3):?>
                <td class="actions">
                    <?= $this->Html->link(__('Edytuj'), ['action' => 'edit', $complaint->id]) ?>
                    <?= $this->Form->postLink(__('Usuń'), ['action' => 'delete', $complaint->id], ['confirm' => __('Are you sure you want to delete # {0}?', $complaint->id)]) ?>
                </td>
              <?php endif?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
