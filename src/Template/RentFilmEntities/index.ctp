 <?php if($user_['permission_id'] ===1|| $user_['permission_id'] ===3): ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <!--<li><?= $this->Html->link(__('New Rent Film Entity'), ['action' => 'add']) ?></li> -->
        <li><?= $this->Html->link(__('Pokaż zamówienia'), ['controller' => 'RentFilms', 'action' => 'index']) ?></li>
      <!--  <li><?= $this->Html->link(__('New Rent Film'), ['controller' => 'RentFilms', 'action' => 'add']) ?></li> -->
        <li><?= $this->Html->link(__('Pokaż filmy'), ['controller' => 'Films', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Dodaj film'), ['controller' => 'Films', 'action' => 'add']) ?></li>
    </ul>
</nav>
<?php endif?>
<div class="rentFilmEntities index large-9 medium-8 columns content">
    <h3><?= __('Składniki zamówienia') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('film_id') ?></th>
                <?php if($user_['permission_id'] ===1 || $user_['permission_id'] === 3): ?>
                <th class="actions"><?= __('Akcje') ?></th>
              <?php endif?>
              <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rentFilmEntities as $rentFilmEntity): ?>
            <tr>
                <td><?= $this->Number->format($rentFilmEntity->id) ?></td>
                <td><?= $rentFilmEntity->has('film') ? $rentFilmEntity->film->nazwa: '' ?></td>
                  <td>TU BYŁBY LINK :)</td>
                 <?php if($user_['permission_id'] ===1 || $user_['permission_id'] === 3): ?>
                <td class="actions">
                    <?= $this->Html->link(__('Edytuj'), ['action' => 'edit', $rentFilmEntity->id]) ?>
                     <?php if($user_['permission_id'] ===1): ?>
                    <?= $this->Form->postLink(__('Usuń'), ['action' => 'delete', $rentFilmEntity->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rentFilmEntity->id)]) ?>
                    <?php endif?>
                </td>
              <?php endif?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
