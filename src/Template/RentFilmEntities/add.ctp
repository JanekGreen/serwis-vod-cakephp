<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Html->link(__('Pokaż filmy'), ['controller' => 'Films', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Dodaj nowy film'), ['controller' => 'Films', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="rentFilmEntities form large-9 medium-8 columns content">
    <?= $this->Form->create($rentFilmEntity) ?>
    <fieldset>
        <legend><?= __('Add Rent Film Entity') ?></legend>
        <?php
            echo $this->Form->input('rent_film_id', ['options' => $rentFilms]);
            echo $this->Form->input('film_id', ['options' => $films]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
