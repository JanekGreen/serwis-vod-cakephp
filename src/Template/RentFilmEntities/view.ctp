<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Rent Film Entity'), ['action' => 'edit', $rentFilmEntity->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rent Film Entity'), ['action' => 'delete', $rentFilmEntity->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rentFilmEntity->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Rent Film Entities'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rent Film Entity'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Rent Films'), ['controller' => 'RentFilms', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rent Film'), ['controller' => 'RentFilms', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Films'), ['controller' => 'Films', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Film'), ['controller' => 'Films', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rentFilmEntities view large-9 medium-8 columns content">
    <h3><?= h($rentFilmEntity->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Rent Film') ?></th>
            <td><?= $rentFilmEntity->has('rent_film') ? $this->Html->link($rentFilmEntity->rent_film->idWypozyczeni, ['controller' => 'RentFilms', 'action' => 'view', $rentFilmEntity->rent_film->idWypozyczenia]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Film') ?></th>
            <td><?= $rentFilmEntity->has('film') ? $this->Html->link($rentFilmEntity->film->nazwa, ['controller' => 'Films', 'action' => 'view', $rentFilmEntity->film->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($rentFilmEntity->id) ?></td>
        </tr>
    </table>
</div>
