<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $rentFilmEntity->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $rentFilmEntity->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Rent Film Entities'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Rent Films'), ['controller' => 'RentFilms', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rent Film'), ['controller' => 'RentFilms', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Films'), ['controller' => 'Films', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Film'), ['controller' => 'Films', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="rentFilmEntities form large-9 medium-8 columns content">
    <?= $this->Form->create($rentFilmEntity) ?>
    <fieldset>
        <legend><?= __('Edit Rent Film Entity') ?></legend>
        <?php
            echo $this->Form->input('rent_film_id', ['options' => $rentFilms]);
            echo $this->Form->input('film_id', ['options' => $films]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
