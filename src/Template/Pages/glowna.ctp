<div id="ramka">
  <ul>
<?php foreach ($genres as $element): ?>
  <li>
     <a href="/VOD/films/<?= $element->id ?>" > <?= $element->opis ?></a>
</li>
<?php endforeach; ?>
</ul>
</div>
<div>
<img src="img/glowna.gif" alt="obrazek" style="height: 50%; width:50%; vertical-align:middle;">
<a href="/VOD/users/register"><h2 style="display:inline;">Zarejestruj się już dziś!</h2><a>
</div>
<br>
<h4>Ostatnio dodany film:</h4>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nazwa</th>
            <th>Reżyser</th>
            <th>Gatunek</th>
            <th>Kraj</th>
            <th>Zniżka</th>
            <th>cena</th>
            <th>Zamów</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $this->Number->format($films->id) ?></td>
            <td><?= h($films->nazwa) ?></td>
            <td><?=$films->director->nazwisko." ".$films->director->imie ?></td>
            <td><?=$films->genre->opis ?></td>
            <td><?=$films->country->Nazwa ?></td>
            <td> <?=$films->discount->nazwa ?></td>
            <td><?= $films->price_type->cena." zł"?></td>
            <td><a href="/VOD/purchases/zamow/<?= $films->id ?>">Zamów</a></td>
        </tr>
    </tbody>
</table>
