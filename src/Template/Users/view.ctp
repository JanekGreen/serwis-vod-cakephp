
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <?php if($user['permission_id']===2):?>
        <li><?= $this->Html->link(__('Pokaż zamówione'), ['controller'=>'RentFilms' ,'action' => 'showUser', $user->username]) ?> </li>
          <li><?= $this->Html->link(__('Pokaż reklamacje'), ['controller'=>'Complaints' ,'action' => 'showUser', $user->id]) ?> </li>
      <?php endif?>
        <li><?= $this->Html->link(__('Edytuj dane'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Skasuj konto'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <?php if($user['permission_id']===1||$user['permission_id']===3 ):?>
        <li><?= $this->Html->link(__('Pokaż użytkowników'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Dodaj użytkownika'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Pokaż uprawnienia'), ['controller' => 'Permissions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nowy typ uprawnień'), ['controller' => 'Permissions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Pokaż prenumeraty'), ['controller' => 'Subscriptions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nowa prenumerata'), ['controller' => 'Subscriptions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Pokaż wszystkie wypożyczone'), ['controller' => 'RentFilms', 'action' => 'index']) ?> </li>
      <!-- <li><?= $this->Html->link(__('New Rent Film'), ['controller' => 'RentFilms', 'action' => 'add']) ?> </li> -->
      <?php endif;?>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->username) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Login') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th><?= __('Hasło') ?></th>
            <td><?= h('***') ?></td>
        </tr>
        <tr>
            <th><?= __('Telefon') ?></th>
            <td><?= h($user->telefon) ?></td>
        </tr>
        <tr>
            <th><?= __('e-mail') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Typ konta') ?></th>
            <td><?=$user->permission->nazwa ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
    </table>

</div>
