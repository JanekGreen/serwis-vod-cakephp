<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Html->link(__('Dodaj użytkownika'), ['action' => 'add']) ?></li>
        <?php if($user_['permission_id']===1):?>
        <li><?= $this->Html->link(__('Pokaż rodzaje uprawnień'), ['controller' => 'Permissions', 'action' => 'index']) ?></li>
      <?php endif ?>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('username') ?></th>
                <th><?= $this->Paginator->sort('telefon') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('permission_id') ?></th>
                <th><?= $this->Paginator->sort('subscription_id') ?></th>
                <th class="actions"><?= __('Akcje') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->username) ?></td>
                <td><?= h($user->telefon) ?></td>
                <td style="font-size:60%;"><?= h($user->email) ?></td>
                <td><?=$user->permission->nazwa?></td>
                <td class="actions">
                  <?php if($user->id!==9):?>
                    <?= $this->Html->link(__('Edytuj'), ['action' => 'edit', $user->id]) ?>
                    <?= $this->Form->postLink(__('Usuń'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                  <?php endif;?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
