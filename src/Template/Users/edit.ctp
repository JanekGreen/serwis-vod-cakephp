<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Form->postLink(
                __('Usuń konto'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
        ?></li>
          <?php if($user_['permission_id']===1 || $user_['permission_id']===3):?>
        <li><?= $this->Html->link(__('Pokaż użytkowników'), ['action' => 'index']) ?></li>
        <?php endif?>
          <?php if($user_['permission_id']===1 ):?>
        <li><?= $this->Html->link(__('Pokaż uprawnienia'), ['controller' => 'Permissions', 'action' => 'index']) ?></li>
          <?php endif?>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edytuj użytkownika') ?></legend>
        <?php
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('telefon');
            echo $this->Form->input('email');
            if($user_['permission_id']===1) {
            echo $this->Form->input('permission_id', ['options' => $permissions]);

          }
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
