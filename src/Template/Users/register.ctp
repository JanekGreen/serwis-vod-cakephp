<div class="row">
    <div class="large-12 columns">


        <?= $this->Flash->render() ?>
        <div class="login-page">

            <div class="large-12 columns">
                <div class="form" id="register-form">
                    <?= $this->Form->create($user222) ?>
                    <fieldset>
                        <legend>Rejestracja konta</legend>
                        <?= $this->Form->input('username',['label' => 'Nazwa użytkownika','required' => true]) ?>
                        <?= $this->Form->input('password',['label' => 'Hasło','required' => true]) ?>
                        <?= $this->Form->input('email',['label' => 'adres e-mail','required' => true]) ?>
                        <?= $this->Form->input('telefon',['label' => 'Numer telefonu','required' => false]) ?>
                        <?= $this->Form->button('Zarejestruj konto', ['type' => 'submit']); ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
