<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Html->link(__('Pokaż użytkowników'), ['action' => 'index']) ?></li>
        <?php if($user_['permission_id']===1):?>
        <li><?= $this->Html->link(__('Pokaż rodzaje uprawnień'), ['controller' => 'Permissions', 'action' => 'index']) ?></li>
        <?php endif?>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Dodaj użytkownika') ?></legend>
        <?php
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('telefon');
            echo $this->Form->input('email');
            if($user_['permission_id']===1)
            echo $this->Form->input('permission_id', ['options' => $permissions]);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Dodaj')) ?>
    <?= $this->Form->end() ?>
</div>
