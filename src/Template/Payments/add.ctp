<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Html->link(__('Pokaż rodzaje płatności'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokaż zamówienia'), ['controller' => 'RentFilms', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowe zamówienie'), ['controller' => 'RentFilms', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="payments form large-9 medium-8 columns content">
    <?= $this->Form->create($payment) ?>
    <fieldset>
        <legend><?= __('Dodaj nowy rodzaj płatności') ?></legend>
        <?php
            echo $this->Form->input('nazwa');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
