<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Payment'), ['action' => 'edit', $payment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Payment'), ['action' => 'delete', $payment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $payment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Payments'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Payment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Rent Films'), ['controller' => 'RentFilms', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rent Film'), ['controller' => 'RentFilms', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="payments view large-9 medium-8 columns content">
    <h3><?= h($payment->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nazwa') ?></th>
            <td><?= h($payment->nazwa) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($payment->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Rent Films') ?></h4>
        <?php if (!empty($payment->rent_films)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('IdWypozyczenia') ?></th>
                <th><?= __('Rent Film Entity Id') ?></th>
                <th><?= __('Termin Wypozyczenia') ?></th>
                <th><?= __('Termin Konca') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Price Type Id') ?></th>
                <th><?= __('Payment Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($payment->rent_films as $rentFilms): ?>
            <tr>
                <td><?= h($rentFilms->idWypozyczenia) ?></td>
                <td><?= h($rentFilms->rent_film_entity_id) ?></td>
                <td><?= h($rentFilms->termin_wypozyczenia) ?></td>
                <td><?= h($rentFilms->termin_konca) ?></td>
                <td><?= h($rentFilms->user_id) ?></td>
                <td><?= h($rentFilms->price_type_id) ?></td>
                <td><?= h($rentFilms->payment_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'RentFilms', 'action' => 'view', $rentFilms->idWypozyczenia]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'RentFilms', 'action' => 'edit', $rentFilms->idWypozyczenia]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'RentFilms', 'action' => 'delete', $rentFilms->idWypozyczenia], ['confirm' => __('Are you sure you want to delete # {0}?', $rentFilms->idWypozyczenia)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
