<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Permission'), ['action' => 'edit', $permission->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Permission'), ['action' => 'delete', $permission->id], ['confirm' => __('Are you sure you want to delete # {0}?', $permission->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Permissions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Permission'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="permissions view large-9 medium-8 columns content">
    <h3><?= h($permission->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nazwa') ?></th>
            <td><?= h($permission->nazwa) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($permission->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($permission->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('IdUzytkownicy') ?></th>
                <th><?= __('Login') ?></th>
                <th><?= __('Haslo') ?></th>
                <th><?= __('Telefon') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Permission Id') ?></th>
                <th><?= __('Subscription Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($permission->users as $users): ?>
            <tr>
                <td><?= h($users->idUzytkownicy) ?></td>
                <td><?= h($users->login) ?></td>
                <td><?= h($users->haslo) ?></td>
                <td><?= h($users->telefon) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->permission_id) ?></td>
                <td><?= h($users->subscription_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->idUzytkownicy]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->idUzytkownicy]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->idUzytkownicy], ['confirm' => __('Are you sure you want to delete # {0}?', $users->idUzytkownicy)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
