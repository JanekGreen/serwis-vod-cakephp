<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Html->link(__('Pokaż rodzaje uprawnień'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokaż użytkowników'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowy użytkownik'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="permissions form large-9 medium-8 columns content">
    <?= $this->Form->create($permission) ?>
    <fieldset>
        <legend><?= __('Dodaj rodzaj Uparawnienia') ?></legend>
        <?php
            echo $this->Form->input('nazwa');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
