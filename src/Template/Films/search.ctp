
<?php if($user_['permission_id']=== 1 || $user_['permission_id']=== 3): ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <ul class="side-nav">
      <li class="heading"><?= __('Akcje') ?></li>
      <li><?= $this->Html->link(__('Pokaż filmy'), ['action' => 'index']) ?></li>
      <li><?= $this->Html->link(__('Pokaż reżyserów'), ['controller' => 'Directors', 'action' => 'index']) ?></li>
      <li><?= $this->Html->link(__('Nowy reżyser'), ['controller' => 'Directors', 'action' => 'add']) ?></li>
      <li><?= $this->Html->link(__('Pokaż gatunki'), ['controller' => 'Genres', 'action' => 'index']) ?></li>
      <li><?= $this->Html->link(__('Nowy gatunek'), ['controller' => 'Genres', 'action' => 'add']) ?></li>
      <li><?= $this->Html->link(__('Pokaż kraje'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
      <li><?= $this->Html->link(__('Nowy kraj'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
      <li><?= $this->Html->link(__('Pokaż zniżki'), ['controller' => 'Discounts', 'action' => 'index']) ?></li>
      <li><?= $this->Html->link(__('Nowa zniżka'), ['controller' => 'Discounts', 'action' => 'add']) ?></li>
      <li><?= $this->Html->link(__('Pokaż ceny'), ['controller' => 'PriceTypes', 'action' => 'index']) ?></li>
      <li><?= $this->Html->link(__('Nowa cena'), ['controller' => 'PriceTypes', 'action' => 'add']) ?></li>
  </ul>
</nav>
<?php endif; ?>
<div class="films index large-9 medium-8 columns content">
    <h3><?= __('Filmy') ?></h3>


    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nazwa') ?></th>
                <th><?= $this->Paginator->sort('director_id','Reżyser') ?></th>
                <th><?= $this->Paginator->sort('genre_id','Gatunek') ?></th>
                <th><?= $this->Paginator->sort('country_id','Kraj') ?></th>
                <th><?= $this->Paginator->sort('discount_id','Zniżka') ?></th>
                <th><?= $this->Paginator->sort('price_type_id','Cena') ?></th>
                <th>Zamów</th>
                  <?php if($user_['permission_id']=== 1 || $user_['permission_id']=== 3): ?>
                <th class="actions"><?= __('Akcje') ?></th>
              <?php endif;?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($films as $film): ?>
            <tr>
                <td><?= $this->Number->format($film->id) ?></td>
                <td><?= h($film->nazwa) ?></td>
                <td><?= $film->director->nazwisko." ".$film->director->imie ?></td>
                <td><?= $film->genre->opis?></td>
                <td><?=$film->country->Nazwa ?></td>
                <td><?=$film->discount->nazwa?></td>
                <?php if($film->discount->id===1):?>
                <td><?=$film->price_type->cena." zł"?></td>
                <?php endif?>
                <?php if($film->discount->id!==1)
                      {
                        $procent=$film->discount->rabat/100;
                        $ile=$film->price_type->cena*$procent;
                        $koszt=$film->price_type->cena-$ile;
                        echo'<td>'.$koszt.' zł</td>';

                      }
                ?>
                <td><a href="/VOD/purchases/zamow/<?= $film->id ?>">Zamów</a></td>
                <?php if($user_['permission_id']=== 1 || $user_['permission_id']=== 3): ?>
                <td class="actions">
                    <?= $this->Html->link(__('Edytuj'), ['action' => 'edit', $film->id]) ?>
                    <?= $this->Form->postLink(__('Usuń'), ['action' => 'delete', $film->id], ['confirm' => __('Are you sure you want to delete # {0}?', $film->id)]) ?>
                </td>
              <?php endif;?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>

    <br>
  <h6><strong>Wyszukiwanie zaawansowane:</strong> </h6>
   <div class="form">
    <?php
    echo $this->Form->create(null, ['action' => 'search']);
    echo $this->Form->input('nazwa');
    echo $this->Form->input('imie');
    echo $this->Form->input('nazwisko');
    echo $this->Form->input('Gatunek', ['options' => $genres,'empty'=>true]);
    echo $this->Form->input('Kraj', ['options' => $countries,'empty'=>true]);
    echo $this->Form->input('Cena', ['options' => $prices,'empty'=>true]);
    echo $this->Form->submit(__('Submit'));
    echo $this->Form->end();
?>
  </div>

</div>
