<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Html->link(__('Pokaż filmy'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokaż reżyserów'), ['controller' => 'Directors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowy reżyser'), ['controller' => 'Directors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Pokaż gatunki'), ['controller' => 'Genres', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowy gatunek'), ['controller' => 'Genres', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Pokaż kraje'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowy kraj'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Pokaż zniżki'), ['controller' => 'Discounts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowa zniżka'), ['controller' => 'Discounts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Pokaż ceny'), ['controller' => 'PriceTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowa cena'), ['controller' => 'PriceTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="films form large-9 medium-8 columns content">
    <?= $this->Form->create($film) ?>
    <fieldset>
        <legend><?= __('Add Film') ?></legend>
        <?php
            echo $this->Form->input('nazwa');
            echo $this->Form->input('director_id', ['options' => $directors]);
            echo $this->Form->input('genre_id', ['options' => $genres]);
            echo $this->Form->input('country_id', ['options' => $countries]);
            echo $this->Form->input('discount_id', ['options' => $discounts]);
            echo $this->Form->input('price_type_id', ['options' => $priceTypes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
