<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $film->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $film->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Films'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Directors'), ['controller' => 'Directors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Director'), ['controller' => 'Directors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Genres'), ['controller' => 'Genres', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Genre'), ['controller' => 'Genres', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Discounts'), ['controller' => 'Discounts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Discount'), ['controller' => 'Discounts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Price Types'), ['controller' => 'PriceTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Price Type'), ['controller' => 'PriceTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Rent Film Entities'), ['controller' => 'RentFilmEntities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rent Film Entity'), ['controller' => 'RentFilmEntities', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="films form large-9 medium-8 columns content">
    <?= $this->Form->create($film) ?>
    <fieldset>
        <legend><?= __('Edit Film') ?></legend>
        <?php
            echo $this->Form->input('nazwa');
            echo $this->Form->input('director_id', ['options' => $directors]);
            echo $this->Form->input('genre_id', ['options' => $genres]);
            echo $this->Form->input('country_id', ['options' => $countries]);
            echo $this->Form->input('discount_id', ['options' => $discounts]);
            echo $this->Form->input('price_type_id', ['options' => $priceTypes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
