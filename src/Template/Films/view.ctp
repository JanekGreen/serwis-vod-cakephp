<?php if($user['permission_id']===2 || $user['permission_id']===3):?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Film'), ['action' => 'edit', $film->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Film'), ['action' => 'delete', $film->id], ['confirm' => __('Are you sure you want to delete # {0}?', $film->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Films'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Film'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Directors'), ['controller' => 'Directors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Director'), ['controller' => 'Directors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Genres'), ['controller' => 'Genres', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Genre'), ['controller' => 'Genres', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Discounts'), ['controller' => 'Discounts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Discount'), ['controller' => 'Discounts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Price Types'), ['controller' => 'PriceTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Price Type'), ['controller' => 'PriceTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Rent Film Entities'), ['controller' => 'RentFilmEntities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rent Film Entity'), ['controller' => 'RentFilmEntities', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<?php endif;?>
<div class="films view large-9 medium-8 columns content">
    <h3><?= h($film->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nazwa') ?></th>
            <td><?= h($film->nazwa) ?></td>
        </tr>
        <tr>
            <th><?= __('Director') ?></th>
            <td><?= $film->has('director') ? $this->Html->link($film->director->nazwisko." ".$film->director->imie, ['controller' => 'Directors', 'action' => 'view', $film->director->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Genre') ?></th>
            <td><?= $film->has('genre') ? $this->Html->link($film->genre->id, ['controller' => 'Genres', 'action' => 'view', $film->genre->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Country') ?></th>
            <td><?= $film->has('country') ? $this->Html->link($film->country->id, ['controller' => 'Countries', 'action' => 'view', $film->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Discount') ?></th>
            <td><?= $film->has('discount') ? $this->Html->link($film->discount->id, ['controller' => 'Discounts', 'action' => 'view', $film->discount->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Price Type') ?></th>
            <td><?= $film->has('price_type') ? $this->Html->link($film->price_type->id, ['controller' => 'PriceTypes', 'action' => 'view', $film->price_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($film->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Rent Film Entities') ?></h4>
        <?php if (!empty($film->rent_film_entities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Film Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($film->rent_film_entities as $rentFilmEntities): ?>
            <tr>
                <td><?= h($rentFilmEntities->id) ?></td>
                <td><?= h($rentFilmEntities->film_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'RentFilmEntities', 'action' => 'view', $rentFilmEntities->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'RentFilmEntities', 'action' => 'edit', $rentFilmEntities->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'RentFilmEntities', 'action' => 'delete', $rentFilmEntities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rentFilmEntities->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
