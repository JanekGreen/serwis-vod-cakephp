<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('pawel.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
  <?php
  $session = $this->request->session();
  $zamowione =$session->read('zamowienie');


   ?>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="large-1 medium-3 columns">
            <li class="name">
               <h1><a href="/VOD/"><img style="width:60px;height:40px;" src="/VOD/img/tv.png" alt="tvlogo" />

</a></h1>
            </li>
        </ul>
        <section class="top-bar-section">
          <ul>
            <li><a href="/VOD/films/">Zobacz wszystko</a>
            <li><a href="/VOD/films/5">Seriale</a>
              <?php if($user_['permission_id']===2):?>
                <li><?php echo '<a href="/VOD/rent-films/show-user/'.$user_['username'].'">Zamówienia </a></li>'; ?>
                <?endif?>
          </ul>
            <ul class="right">
                <?php if(!$user_): ?>
                   <li><a href="/VOD/users/login">Zaloguj</a></li></li>
                   <?php else: ?>
                 <li><a href="/VOD/users/logout">Wyloguj</a></li></li>
                <?php echo  '<li><a href="/VOD/users/view/' .$user_['id'] .'">' .$user_['username'] .'</a></li>'; ?>
                    <?php if($user_['permission_id']===2):?>
                        <li><a href="/VOD/purchases">Koszyk(<?= count($zamowione) ?>)</a>
                         <?php endif?>
                    <?php if($user_['permission_id']===1):?>
                          <li><a href="/VOD/pages/admin">Panel admina</a>
                            <?php endif?>
                     <?php if($user_['permission_id']===3):?>
                          <li><a href="/VOD/pages/pracownik">Panel pracownika</a>
                          <?php endif?>

               <?php endif ?>

            </ul>
        </section>
    </nav>
    <?= $this->Flash->render() ?>
    <section class="container clearfix">
        <?= $this->fetch('content') ?>
    </section>
    <footer>
    </footer>
</body>
</html>
