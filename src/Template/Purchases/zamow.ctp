<?php
 $session = $this->request->session();
  $zamowione =$session->read('zamowienie');
  $znizka=false;
  ?>
  <table cellpadding="0" cellspacing="0">
      <thead>
          <tr>
            <th>ID</th>
            <th>Nazwa filmu</th>
            <th>Usuń</th>
          </tr>
       </thead>
<?php
 if($zamowione!==null):
   $doZaplaty=0;
  foreach ($zamowione as $film):?>
  <tr>
    <td><?= $film->id ?></td>
    <td><?= $film->nazwa ?></td>
    <td><a href="/VOD/purchases/usun/<?=$film->id ?>">Usuń</a></td>
    <?php
     if($film->discount->id === 1)
    $doZaplaty+=$film->price_type->cena;
    else {
      $znizka=true;
      $procent=$film->discount->rabat/100;
      $ile=$film->price_type->cena*$procent;
      $doZaplaty+=($film->price_type->cena-$ile);
      echo'<td style="color:red; font-weight:bold"> zniżka '. $film->discount->rabat.' %</td>';
    }
    ?>
  </tr>
<?php endforeach ?>
<?php endif?>

</table>
<div id="ramka">
<ul>
<li><a href="/VOD/purchases/anuluj">Anuluj zamówienie</a></li>
<li><a href="/VOD/purchases/podsumowanie">Realizuj zamówienie</a></li>
<li><a href="/VOD/films/">Zamawiaj dalej</a></li>
</ul>
<li style="color:white; background-color:red" >Do zapłaty: <?= $zamowione!==null ?$doZaplaty: '0' ?> zł</li>
</div>
