<h2>Podsumowanie</h2>
<?php
use Cake\I18n\Time;
Time::setToStringFormat('yyyy-MM-dd');
 $teraz= Time::now();
 $session = $this->request->session();
  $zamowione =$session->read('zamowienie');
  ?>
  <table cellpadding="0" cellspacing="0">
      <thead>
          <tr>
            <th>Nazwa filmu</th>
          </tr>
       </thead>
<?php
 if($zamowione!==null):
   $doZaplaty=0;
  foreach ($zamowione as $film):?>
  <tr>
    <td><?= $film->nazwa ?></td>
    <?php
    if($film->discount->id === 1)
   $doZaplaty+=$film->price_type->cena;
   else {
     $znizka=true;
     $procent=$film->discount->rabat/100;
     $ile=$film->price_type->cena*$procent;
     $doZaplaty+=($film->price_type->cena-$ile);
     echo'<td style="color:red; font-weight:bold"> zniżka '. $film->discount->rabat.' %</td>';
   }

    ?>
  </tr>
<?php endforeach ?>

<?php endif?>
</table>
<div class="films form large-9 medium-8 columns content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Szczegóły zamówienia') ?></legend>
         Użytkownik: <strong><?=$user_['username']?></strong> <br>
         Kwota: <strong><?=$doZaplaty ?>zł </strong> <br>
         Koniec dostępu do linka: <strong><?=$teraz->modify('+5 days');?></strong> <br>
         <hr>
        <?php

            echo $this->Form->input('Sposób zapłaty', ['options' => $payments]); ?>
                '<input type="hidden" name="kwota" value="<?=$doZaplaty?>">


    </fieldset>
    <?= $this->Form->button(__('Zamów')) ?>
    <?= $this->Form->end() ?>
</div>

<div id="ramka">
<ul>
<li><a href="/VOD/purchases/anuluj">Anuluj zamówienie</a></li>
</ul>
<li style="color:white; background-color:red" >Do zapłaty: <?= $zamowione!==null ?$doZaplaty: '0' ?> zł</li>
</div>
