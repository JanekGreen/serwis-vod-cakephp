<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Rent Film'), ['action' => 'edit', $rentFilm->idWypozyczenia]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rent Film'), ['action' => 'delete', $rentFilm->idWypozyczenia], ['confirm' => __('Are you sure you want to delete # {0}?', $rentFilm->idWypozyczenia)]) ?> </li>
        <li><?= $this->Html->link(__('List Rent Films'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rent Film'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Payments'), ['controller' => 'Payments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Payment'), ['controller' => 'Payments', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rentFilms view large-9 medium-8 columns content">
    <h3><?= h($rentFilm->idWypozyczeni) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $rentFilm->has('user') ? $this->Html->link($rentFilm->user->username, ['controller' => 'Users', 'action' => 'view', $rentFilm->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Payment') ?></th>
            <td><?= $rentFilm->has('payment') ? $this->Html->link($rentFilm->payment->nazwa, ['controller' => 'Payments', 'action' => 'view', $rentFilm->payment->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($rentFilm->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Cena') ?></th>
            <td><?= $this->Number->format($rentFilm->cena) ?></td>
        </tr>
        <tr>
            <th><?= __('Termin Wypozyczenia') ?></th>
            <td><?= h($rentFilm->termin_wypozyczenia) ?></td>
        </tr>
        <tr>
            <th><?= __('Termin Konca') ?></th>
            <td><?= h($rentFilm->termin_konca) ?></td>
        </tr>
    </table>
</div>
