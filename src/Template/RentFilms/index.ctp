<?php use Cake\I18n\Time; ?>
 <?php if($user_['permission_id'] ===1|| $user_['permission_id'] ===3): ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Akcje') ?></li>
        <li><?= $this->Html->link(__('Pokaż użytkowników'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nowy użytkownik'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<?php endif?>
<div class="rentFilms index large-9 medium-8 columns content">
    <h3><?= __('Zamówienia') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('termin_wypozyczenia') ?></th>
                <th><?= $this->Paginator->sort('termin_konca') ?></th>
                <th><?= $this->Paginator->sort('user_id','Użytkownik') ?></th>
                <th><?= $this->Paginator->sort('cena') ?></th>
                <th><?= $this->Paginator->sort('payment_id','Rodzaj płatności') ?></th>
                <th>Zamówione filmy</th>
                <?php if($user_['permission_id'] ===2):?>
                  <th>Zgłoś reklamacje</th>
                <?php endif?>
                 <?php if($user_['permission_id'] ===1|| $user_['permission_id'] ===3): ?>
                <th class="actions"><?= __('Actions') ?></th>
              <?php endif?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rentFilms as $rentFilm): ?>
            <tr>
                <td><?= $this->Number->format($rentFilm->id) ?></td>
                <td><?= h($rentFilm->termin_wypozyczenia) ?></td>
                <td><?= h($rentFilm->termin_konca) ?></td>
                <td><?= $rentFilm->has('user') ?$rentFilm->user->username: '' ?></td>
                <td><?= $this->Number->format($rentFilm->cena) ?></td>
                <td><?= $rentFilm->has('payment') ? $rentFilm->payment->nazwa : '' ?></td>
                <?php
                Time::setToStringFormat('yy.MM.dd');
                $dataKonca=  new Time($rentFilm->termin_konca);

                if($dataKonca<  date("y.m.d"))
                {
                  echo '<td>Zrealizowane</td>';
                }
                else {
                  echo'<td><a href="/VOD/rent-film-entities/'.$rentFilm->id.'">Pokaż </a></td>';
                }
                ?>
                <?php if($user_['permission_id'] ===2):?>
                  <td><?=$this->Html->link(__('Kliknij'), ['controller'=>'Purchases', 'action' => 'reklamuj', $rentFilm->id])  ?></td>
                <?php endif?>
                 <?php if($user_['permission_id'] ===1|| $user_['permission_id'] ===3): ?>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rentFilm->id]) ?>
                     <?php if($user_['permission_id'] ===1): ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rentFilm->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rentFilm->id)]) ?>
                    <?php endif?>
                </td>
              <?php endif?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
