<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Rent Films'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Payments'), ['controller' => 'Payments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Payment'), ['controller' => 'Payments', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="rentFilms form large-9 medium-8 columns content">
    <?= $this->Form->create($rentFilm) ?>
    <fieldset>
        <legend><?= __('Add Rent Film') ?></legend>
        <?php
            echo $this->Form->input('id');
            echo $this->Form->input('termin_wypozyczenia', ['empty' => true]);
            echo $this->Form->input('termin_konca', ['empty' => true]);
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('cena');
            echo $this->Form->input('payment_id', ['options' => $payments]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
