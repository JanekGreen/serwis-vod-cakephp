
<?php if($user===2 || $user===null):?>
<nav class="large-3 medium-4 columns " id="actions-sidebar" >
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Genre'), ['action' => 'edit', $genre->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Genre'), ['action' => 'delete', $genre->id], ['confirm' => __('Are you sure you want to delete # {0}?', $genre->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Genres'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Genre'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Films'), ['controller' => 'Films', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Film'), ['controller' => 'Films', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<?php endif; ?>
<div class="genres view large-9 medium-8 columns content">
    <h3><?= h($genre->opis) ?></h3>

    <div class="related">
        <?php if (!empty($genre->films)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nazwa') ?></th>
                <th><?= __('Director Id') ?></th>
                <th><?= __('Genre Id') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Discount Id') ?></th>
                <th><?= __('Price Type Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($genre->films as $films): ?>
            <tr>
                <td><?= h($films->id) ?></td>
                <td><?= h($films->nazwa) ?></td>
                <td><?= h($films->director_id) ?></td>
                <td><?= h($films->genre_id) ?></td>
                <td><?= h($films->country_id) ?></td>
                <td><?= h($films->discount_id) ?></td>
                <td><?= h($films->price_type_id) ?></td>
                <td class="actions  <?php if(h($user['permission_id']) ==2 || h($user['permission_id']) ==null) echo 'ukryj'; ?>"  >
                    <?= $this->Html->link(__('View'), ['controller' => 'Films', 'action' => 'view', $films->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Films', 'action' => 'edit', $films->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Films', 'action' => 'delete', $films->id], ['confirm' => __('Are you sure you want to delete # {0}?', $films->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
