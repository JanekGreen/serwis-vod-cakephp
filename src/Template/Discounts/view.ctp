<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Discount'), ['action' => 'edit', $discount->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Discount'), ['action' => 'delete', $discount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $discount->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Discounts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Discount'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Films'), ['controller' => 'Films', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Film'), ['controller' => 'Films', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="discounts view large-9 medium-8 columns content">
    <h3><?= h($discount->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nazwa') ?></th>
            <td><?= h($discount->nazwa) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($discount->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Rabat') ?></th>
            <td><?= $this->Number->format($discount->rabat) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Films') ?></h4>
        <?php if (!empty($discount->films)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nazwa') ?></th>
                <th><?= __('Director Id') ?></th>
                <th><?= __('Genre Id') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Discount Id') ?></th>
                <th><?= __('Price Type Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($discount->films as $films): ?>
            <tr>
                <td><?= h($films->id) ?></td>
                <td><?= h($films->nazwa) ?></td>
                <td><?= h($films->director_id) ?></td>
                <td><?= h($films->genre_id) ?></td>
                <td><?= h($films->country_id) ?></td>
                <td><?= h($films->discount_id) ?></td>
                <td><?= h($films->price_type_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Films', 'action' => 'view', $films->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Films', 'action' => 'edit', $films->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Films', 'action' => 'delete', $films->id], ['confirm' => __('Are you sure you want to delete # {0}?', $films->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
