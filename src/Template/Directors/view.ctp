<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Director'), ['action' => 'edit', $director->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Director'), ['action' => 'delete', $director->id], ['confirm' => __('Are you sure you want to delete # {0}?', $director->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Directors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Director'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Films'), ['controller' => 'Films', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Film'), ['controller' => 'Films', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="directors view large-9 medium-8 columns content">
    <h3><?= h($director->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Imie') ?></th>
            <td><?= h($director->imie) ?></td>
        </tr>
        <tr>
            <th><?= __('Nazwisko') ?></th>
            <td><?= h($director->nazwisko) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($director->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Films') ?></h4>
        <?php if (!empty($director->films)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nazwa') ?></th>
                <th><?= __('Director Id') ?></th>
                <th><?= __('Genre Id') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Discount Id') ?></th>
                <th><?= __('Price Type Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($director->films as $films): ?>
            <tr>
                <td><?= h($films->id) ?></td>
                <td><?= h($films->nazwa) ?></td>
                <td><?= h($films->director_id) ?></td>
                <td><?= h($films->genre_id) ?></td>
                <td><?= h($films->country_id) ?></td>
                <td><?= h($films->discount_id) ?></td>
                <td><?= h($films->price_type_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Films', 'action' => 'view', $films->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Films', 'action' => 'edit', $films->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Films', 'action' => 'delete', $films->id], ['confirm' => __('Are you sure you want to delete # {0}?', $films->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
