<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Film Entity.
 *
 * @property int $id
 * @property string $nazwa
 * @property int $director_id
 * @property \App\Model\Entity\Director $director
 * @property int $genre_id
 * @property \App\Model\Entity\Genre $genre
 * @property int $country_id
 * @property \App\Model\Entity\Country $country
 * @property int $discount_id
 * @property \App\Model\Entity\Discount $discount
 * @property int $price_type_id
 * @property \App\Model\Entity\PriceType $price_type
 * @property \App\Model\Entity\RentFilmEntity[] $rent_film_entities
 */
class Film extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
