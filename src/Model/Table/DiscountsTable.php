<?php
namespace App\Model\Table;

use App\Model\Entity\Discount;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Discounts Model
 *
 * @property \Cake\ORM\Association\HasMany $Films
 */
class DiscountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('discounts');
        $this->displayField('nazwa');
        $this->primaryKey('id');

        $this->hasMany('Films', [
            'foreignKey' => 'discount_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('nazwa');

        $validator
            ->add('rabat', 'valid', ['rule' => 'decimal'])
            ->notEmpty('rabat');

        return $validator;
    }
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['nazwa']));
        $rules->add($rules->isUnique(['rabat']));
        return $rules;
    }

}
