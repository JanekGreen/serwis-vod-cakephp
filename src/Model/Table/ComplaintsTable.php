<?php
namespace App\Model\Table;

use App\Model\Entity\Complaint;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Complaints Model
 *
 * @property \Cake\ORM\Association\BelongsTo $RentFilms
 */
class ComplaintsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('complaints');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('RentFilms', [
            'foreignKey' => 'rent_film_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('opis');

        $validator
            ->add('zalatwione', 'valid', ['rule' => 'boolean'])
            ->requirePresence('zalatwione', 'create')
            ->notEmpty('zalatwione');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['rent_film_id'], 'RentFilms'));
        return $rules;
    }
}
