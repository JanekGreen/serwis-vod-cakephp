<?php
namespace App\Model\Table;

use App\Model\Entity\Director;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Directors Model
 *
 * @property \Cake\ORM\Association\HasMany $Films
 */
class DirectorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('directors');
        $this->displayField(['id','nazwisko','imie']);
        $this->primaryKey('id');

        $this->hasMany('Films', [
            'foreignKey' => 'director_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('imie');

        $validator
            ->notEmpty('nazwisko');

        return $validator;
    }
}
