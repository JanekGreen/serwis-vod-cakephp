<?php
namespace App\Model\Table;

use App\Model\Entity\PriceType;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PriceTypes Model
 *
 * @property \Cake\ORM\Association\HasMany $Films
 * @property \Cake\ORM\Association\HasMany $RentFilms
 */
class PriceTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('price_types');
        $this->displayField('cena');
        $this->primaryKey('id');

        $this->hasMany('Films', [
            'foreignKey' => 'price_type_id'
        ]);
        $this->hasMany('RentFilms', [
            'foreignKey' => 'price_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('nazwa');

        $validator
            ->add('cena', 'valid', ['rule' => 'numeric'])
            ->notEmpty('cena');

        return $validator;
    }
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['cena']));
        return $rules;
    }
}
