<?php
namespace App\Model\Table;

use App\Model\Entity\Film;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Films Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Directors
 * @property \Cake\ORM\Association\BelongsTo $Genres
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\BelongsTo $Discounts
 * @property \Cake\ORM\Association\BelongsTo $PriceTypes
 * @property \Cake\ORM\Association\HasMany $RentFilmEntities
 */
class FilmsTable extends Table
{

  public $filterArgs = array(
    'nazwa' => array(
        'type' => 'like',
        'field' => 'nazwa'
  ),
  'nazwisko' => array(
            'type' => 'like',
            'field' => 'Directors.nazwisko'
),
  'imie' => array(
          'type' => 'like',
          'field' => 'Directors.imie'
        ),
        'Kraj' => array(
                'type' => 'like',
                'field' => 'Countries.id'
              ),
              'Cena' => array(
                      'type' => 'like',
                      'field' => 'Countries.id'
            ),
            'Gatunek' => array(
                    'type' => 'like',
                    'field' => 'Genres.id'
          )

      );

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Search.Searchable');

        $this->table('films');
        $this->displayField('nazwa');
        $this->primaryKey('id');

        $this->belongsTo('Directors', [
            'foreignKey' => 'director_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Genres', [
            'foreignKey' => 'genre_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Discounts', [
            'foreignKey' => 'discount_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PriceTypes', [
            'foreignKey' => 'price_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('RentFilmEntities', [
            'foreignKey' => 'film_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nazwa','discount_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['director_id'], 'Directors'));
        $rules->add($rules->existsIn(['genre_id'], 'Genres'));
        $rules->add($rules->existsIn(['country_id'], 'Countries'));
        $rules->add($rules->existsIn(['discount_id'], 'Discounts'));
        $rules->add($rules->existsIn(['price_type_id'], 'PriceTypes'));
        $rules->add($rules->isUnique(['nazwa']));
        return $rules;
    }



 }
